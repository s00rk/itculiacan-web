window.importante = [];
window.noticias = [];

$(document).on('ready', function(){

	actualizar();

	$('#home>div>a').on('click', function (){
		$('ul').empty();
		actualizar();
	});

	$('ul').on('click', 'li', function (e){
		e.preventDefault();
		$('#contenido').html( '' );
		$.mobile.changePage('#noticia');
		$.mobile.loading( 'show', { text: 'Cargando [ ' + $(this).attr('data-title') + ' ]', textVisible: true } );
		var url = $(this).attr('data-url');		
		$.getJSON('http://whateverorigin.org/get?url=' + encodeURIComponent(url) + '&callback=?', function(data){			
			data = data.contents;
			data = $('.single_post_module', data);
			var src = $('.single_post_image img', data).attr('src');
			var btnBack = '<a href="#" data-role="button" data-rel="back" data-icon="back">Regresar</a>';

			src = '<img id="img_noticia" src="' + src + '" />';
			var width = $('#contenido').width()-15;

			$('#contenido').html( btnBack + src );
			$('#contenido').append( data );
			$('#contenido .single_post_image').css('display', 'none');
			$('#contenido .post_meta_bottom').css('display', 'none');
			$('#contenido .post_meta').css('display', 'none');
			$('#contenido img').css('max-height', '15em');

			$('#contenido').trigger('create');

			$.mobile.loading( 'hide' );
		});
	});

});


function actualizar()
{
	$.mobile.loading( 'show', { text: 'Cargando noticias', textVisible: true }  );
	$('#home>div>a').addClass('ui-disabled');
	$.getJSON('http://whateverorigin.org/get?url=' + encodeURIComponent('http://itculiacan.edu.mx') + '&callback=?', function(data){//$.get('web.html', function (data){
		data = data.contents;
		var importante_ = $('.post_grid', data);
		var noticias_ = importante_.eq(1);
		importante_ = importante_.eq(0);
		importante_.find('.one_third').each(function (i, data){
			var obj = { 
				'title': $('.post_title>a', data).text(),
				'link': $('.post_title>a', data).attr('href'),
				'image': $('.post_grid_image>a>span>img', data).attr('src'),
				'desc': $('.post_grid_content>div>p:first', data).text()
			};
			importante.push( obj );
			agregarLista('importante', obj);
		});
		$('#importante_collapse').trigger('expand');
		noticias_.find('.one_third').each(function (i, data){
			var obj = { 
				'title': $('.post_title>a', data).text(),
				'link': $('.post_title>a', data).attr('href'),
				'image': $('.post_grid_image>a>span>img', data).attr('src'),
				'desc': $('.post_grid_content>div>p:first', data).text()
			};
			noticias.push( obj );
			agregarLista('noticia', obj);
		});
		$('#noticia_collapse').trigger('expand');
		$('#home>div>a').removeClass('ui-disabled');
		$.mobile.loading( 'hide' );
	});	
}
function agregarLista(li, obj){
	var tpl = $('#lista').html();
	var output = swig.render(tpl, { locals: { image: obj.image, title: obj.title, desc: obj.desc, link: obj.link }});
	$('#' + li).append(output);
	$('#' + li).listview('refresh');
}